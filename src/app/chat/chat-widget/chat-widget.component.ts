import { Component, OnInit, ViewChild, Input, ElementRef } from '@angular/core';
import { fadeIn, fadeInOut } from '../animations'
import { Subject } from 'rxjs/Subject';


const randomMessages: Array<string> = [
  'Nice to meet you',
  'How are you?',
  'Not too bad, thanks',
  'What do you do?',
  'Is there anything else I can help you with?',
  'That\'s awesome',
  'Angular Elements is the bomb 💣 ',
  'Can you explain in more detail?',
  'Anyway I\'ve gotta go now',
  'It was a pleasure to chat with you',
  'We are happy to make you a custom offer!',
  'Bye',
  ':)'
];

const rand = max => Math.floor(Math.random() * max);
const getRandomMessage = () => randomMessages[rand(randomMessages.length)];

@Component({
  selector: 'chat-widget',
  templateUrl: './chat-widget.component.html',
  styleUrls: ['./chat-widget.component.css'],
  animations: [fadeInOut, fadeIn]

})
export class ChatWidgetComponent implements OnInit {

  @ViewChild('bottom') bottom: ElementRef;
  @Input() public theme: 'blue' | 'grey' | 'red' = 'blue';

  public _visible = false;
  public focus = new Subject();
  public messages = [];

  public get visible() {
    return this._visible;
  }

  @Input() public set visible(visible) {
    this._visible = visible;
    if (this._visible) {
      setTimeout(() => {
        this.scrollToBottom();
      });
    }
  }

  public operator = {
    name: 'Operator',
    status: 'online',
    avatar: `https://randomuser.me/api/portraits/women/${this.rand(100)}.jpg`,
  }

  public client = {
    name: 'Guest User',
    status: 'online',
    avatar: `https://randomuser.me/api/portraits/men/${this.rand(100)}.jpg`,
  }


  constructor() { }

  ngOnInit() {
  }

  rand(max) {
    return Math.floor(Math.random() * 100);
  }

  scrollToBottom() {
    if (this.bottom !== undefined) {
      this.bottom.nativeElement.scrollIntoView();
    }
  }

  toggleChat() {
    this.visible = !this.visible;
  }

  public focusMessage() {
    this.focus.next(true);
  }
  sendMessage({ message }) {
    if (message.trim() === '') {
      return null;
    }
    console.log(message);
    this.addMessage(this.client, message, 'sent');
    setTimeout(() => this.randomMessage(), 2000);
    console.log("Messages :" + this.messages);
  }

  randomMessage() {
    this.addMessage(this.operator, getRandomMessage(), 'received');
  }

  public addMessage(from, text, type: 'received' | 'sent') {
    this.messages.unshift({
      from,
      text,
      type,
      date: new Date().getTime()
    });

    this.scrollToBottom();
  }
}
