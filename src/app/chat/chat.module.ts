import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatAvatorComponent } from './chat-avator/chat-avator.component';
import { ChatConfigComponent } from './chat-config/chat-config.component';
import { ChatInputComponent } from './chat-input/chat-input.component';
import { ChatWidgetComponent } from './chat-widget/chat-widget.component';

@NgModule({
    declarations: [ChatAvatorComponent, ChatConfigComponent, ChatInputComponent, ChatWidgetComponent],
    imports: [CommonModule],
    exports: [ChatWidgetComponent,ChatConfigComponent],
    providers: [],
})
export class ChatModule { }