import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'chat-avatar',
    template: `
    <img [attr.src]="image" alt="chat-image" class="avator"/>
    `,
    styles : [
        `
        .avator{
            width:  30px;
            height: 30px;
            border-radius: 50%;
            margin-right: 10px;
            float: left;
        }
        `
    ]
})
export class ChatAvatorComponent{

    @Input() public image: string;
}
