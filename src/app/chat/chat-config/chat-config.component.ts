import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'chat-config',
  templateUrl: './chat-config.component.html',
  styleUrls: ['./chat-config.component.css']
})
export class ChatConfigComponent implements OnInit {

  @Input() public theme: string;
  @Input() public text = 'Select Theme';
  @Output() public themeChange: EventEmitter<any> = new EventEmitter();
  constructor() { }

  public themes=['blue','green','gray','red'];
  

  ngOnInit() {
  }

  public setTheme(theme) {
    this.theme = theme;
    this.themeChange.emit(this.theme);
  }
}
