import { Component, OnInit, EventEmitter, Output, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'chat-input',
  templateUrl: './chat-input.component.html',
  styleUrls: ['./chat-input.component.css']
})
export class ChatInputComponent implements OnInit {

  @Input() public buttonText = '↩︎';
  @Input() public focus = new EventEmitter();
  @Output() public send = new EventEmitter();
  @Output() public dismiss = new EventEmitter();
  @ViewChild('message') message: ElementRef;

  constructor() { }

  ngOnInit() {
    this.focus.subscribe(() => this.focusMessage());
  }

  getMessage() {
    return this.message.nativeElement.value;
  }

  clearMessage() {
    this.message.nativeElement.value = '';
  }

  onSubmit() {
    const message: string = this.getMessage();
    if (message.trim() === '') {
      return;
    }
    this.send.emit({ message });
    this.clearMessage();
    this.focusMessage();
  }

  public focusMessage() {
    this.message.nativeElement.focus();
  }
}
